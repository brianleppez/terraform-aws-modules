#===============================================================================
# Locals
#===============================================================================
locals {
  common_tags = "${map(
    "ManagedBy", "terraform",
    "Environment", "${var.environment_name}",
    "StackID", "${var.stack_id}",
  )}"
}

#===============================================================================
# KMS
#===============================================================================
resource "aws_kms_key" "sops" {
  description             = "${var.environment_name}-${var.stack_id} sops encryption key"
  deletion_window_in_days = 30

  tags = "${local.common_tags}"
}

resource "aws_kms_alias" "sops" {
  name          = "alias/kms/${var.environment_name}-${var.stack_id}-sops"
  target_key_id = "${aws_kms_key.sops.key_id}"
}
