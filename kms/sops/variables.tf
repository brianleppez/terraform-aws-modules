variable "aws_region" {
  description = "Deploy to this AWS region"
}

variable "environment_name" {
  description = "The name of the environment"
}

variable "stack_id" {
  description = "The stack id within the configured environment. Used to differentiate stacks of the same environment name"
}
