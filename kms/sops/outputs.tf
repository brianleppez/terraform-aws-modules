output "sops_kms_key_arn" {
  description = "The KMS key id used for sops"
  value       = "${aws_kms_key.sops.arn}"
}
