output "vpc_id" {
  value = "${aws_vpc.brian.id}"
}

output "vpc_name" {
  value = "${local.vpc_name}"
}

output "vpc_ipv4_cidr" {
  value = "${var.ipv4_cidr}"
}

output "vpc_fqdn" {
  value = "${local.vpc_fqdn}"
}

output "vpc_internet_gateway_id" {
  value = "${aws_internet_gateway.brian_vpc.id}"
}

output "public_subnet_ids" {
  value = "${aws_subnet.public.*.id}"
}

output "public_route53_zone_id" {
  value = "${aws_route53_zone.public.zone_id}"
}

output "kubernetes_security_group_id" {
  value = "${aws_security_group.vpn.id}"
}
