data "aws_route53_zone" "parent" {
  # the Route53 hosted zone for the parent domain. e.g. brianleppez.com
  name = "${var.environment_domain}"
}
