variable "aws_region" {
  description = "Deploy to this AWS region"
}

variable "environment_name" {
  description = "The name of the environment"
}

variable "stack_id" {
  description = "The stack id within the configured environment. Used to differentiate stacks of the same environment name"
}

variable "environment_domain" {
  description = "The root domain name of the environment"
}

variable "provisioner_public_key" {
  description = "The SSH public key that will be used for provisioning EC2 instances within the VPC. Unique to the environment (don't reuse across environments)"
}

variable "public_availability_zones" {
  description = "Create public subnets in these availability zones"
  type        = "list"
}

variable "public_subnet_cidrs" {
  description = "Assign these CIDR blocks to the public subnets in the availability zones above, in order"
  type        = "list"
}

variable "admin_availability_zones" {
  description = "Create administration subnets in these availability zones. These are for VPN instances, etc."
  type        = "list"
}

variable "admin_subnet_cidrs" {
  description = "Assign these CIDR blocks to the admin subnets in the availability zones above, in order"
  type        = "list"
}

variable "ipv4_cidr" {
  description = "The IPv4 CIDR block to assign to the VPC"
}

variable "ami_id" {
  description = "The AMI ID to use. This is not dynamic, otherwise a terraform apply will destroy the instance once an update is released"
}

variable "instance_type" {
  description = "The EC2 instance type to create"
}
