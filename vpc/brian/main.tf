#===============================================================================
# Locals
#===============================================================================
locals {
  vpc_name = "${var.environment_name}-${var.stack_id}-brian"

  vpc_fqdn = "${var.environment_name}-${var.stack_id}.${var.environment_domain}"

  common_tags = "${map(
    "ManagedBy", "Terraform",
    "Environment", "${var.environment_name}",
    "StackID", "${var.stack_id}"
  )}"
}

#===============================================================================
# Keypair
#===============================================================================
resource "aws_key_pair" "provisioner" {
  key_name   = "${var.environment_name}-${var.stack_id}-provisioner"
  public_key = "${var.provisioner_public_key}"
}

#===============================================================================
# VPC
#===============================================================================
resource "aws_vpc" "brian" {
  cidr_block           = "${var.ipv4_cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = "${merge(
    local.common_tags,
    map("Name", "${local.vpc_name}"),
  )}"
}

#===============================================================================
# Internet Gateway
#===============================================================================
resource "aws_internet_gateway" "brian_vpc" {
  vpc_id = "${aws_vpc.brian.id}"

  tags = "${merge(
    local.common_tags,
    map("Name", "${local.vpc_name}")
  )}"
}

#===============================================================================
# NAT Instance
#===============================================================================
resource "aws_instance" "nat" {
  count                       = "${length(var.public_availability_zones)}"
  availability_zone           = "${element(var.public_availability_zones, count.index)}"
  ami                         = "${var.ami_id}"                                          # "ami-6975eb1e" # amzn-ami-vpc-nat-hvm-2015.03.0.x86_64-gp2
  instance_type               = "${var.instance_type}"
  key_name                    = "${aws_key_pair.provisioner}"
  security_groups             = ["${aws_security_group.nat.id}"]
  subnet_id                   = "${aws_subnet.public.0.id}"
  associate_public_ip_address = true
  source_dest_check           = false

  tags = "${merge(
    local.common_tags,
    map(
      "Name", "${var.environment_name}-${var.stack_id}-vpn",
      "Role", "vpn",
    ),
  )}"
}

resource "aws_eip" "nat" {
  vpc        = true
  instance   = "${aws_instance.nat.id}"
  depends_on = ["aws_instance.nat"]

  tags = "${merge(
    local.common_tags,
    map("Name", "${var.environment_name}-${var.stack_id}-vpn"),
  )}"
}

resource "aws_cloudwatch_metric_alarm" "nat-1a_recover" {
  alarm_name          = "${var.region}-${var.vpc_name}-nat-1a-recover"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "StatusCheckFailed_System"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Minimum"
  threshold           = "0"
  alarm_actions       = ["arn:aws:automate:${var.region}:ec2:recover"]

  dimensions = {
    InstanceId = "${aws_instance.nat-1a.id}"
  }

  depends_on = ["aws_instance.nat-1a"]
}

#===============================================================================
# Route53
#===============================================================================
resource "aws_route53_zone" "public" {
  name = "${local.vpc_fqdn}"

  tags = "${merge(
    local.common_tags,
    map("Name", "${local.vpc_fqdn}"),
  )}"
}

resource "aws_route53_record" "parent_zone_nameserver_records" {
  zone_id = "${data.aws_route53_zone.parent.zone_id}"
  name    = "${local.vpc_fqdn}"
  type    = "NS"
  ttl     = "300"
  records = ["${aws_route53_zone.public.name_servers}"]
}

#===============================================================================
# Subnets
#===============================================================================
resource "aws_subnet" "public" {
  count             = "${length(var.public_availability_zones)}"
  availability_zone = "${element(var.public_availability_zones, count.index)}"
  cidr_block        = "${element(var.public_subnet_cidrs, count.index)}"
  vpc_id            = "${aws_vpc.brian.id}"

  # kops and kubernetes add tags to subnets. Ignore them so Terraform doesn't remove them
  lifecycle {
    ignore_changes = [
      "tags.%",
      "tags.kubernetes",
      "tags.SubnetType",
    ]
  }

  tags = "${merge(
    local.common_tags,
    map("Name", "${var.environment_name}-${var.stack_id}-public-${element(var.public_availability_zones, count.index)}"),
  )}"
}

resource "aws_subnet" "admin" {
  count             = "${length(var.admin_availability_zones)}"
  availability_zone = "${element(var.admin_availability_zones, count.index)}"
  cidr_block        = "${element(var.admin_subnet_cidrs, count.index)}"
  vpc_id            = "${aws_vpc.brian.id}"

  tags = "${merge(
    local.common_tags,
    map("Name", "${var.environment_name}-${var.stack_id}-admin-${element(var.public_availability_zones, count.index)}"),
  )}"
}

#===============================================================================
# Route Tables
#===============================================================================
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.brian.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.brian_vpc.id}"
  }

  depends_on = ["aws_internet_gateway.brian_vpc"]

  tags = "${merge(
    local.common_tags,
    map("Name", "${local.vpc_name}-public")
  )}"
}

resource "aws_route_table_association" "public_subnet_igw" {
  # Intentionally using the length of var.public_availability_zones for count
  # The value of count can't be computed from aws_subnet.public above
  count = "${length(var.public_availability_zones)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"

  depends_on = ["aws_subnet.public"]
}

resource "aws_route_table" "admin" {
  vpc_id = "${aws_vpc.brian.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.brian.id}"
  }

  tags = "${merge(
    local.common_tags,
    map("Name", "${local.vpc_name}-admin")
  )}"
}

resource "aws_route_table_association" "admin_subnet_igw" {
  # Intentionally using the length of var.admin_availability_zones for count
  # The value of count can't be computed from aws_subnet.admin above
  count = "${length(var.admin_availability_zones)}"

  subnet_id      = "${element(aws_subnet.admin.*.id, count.index)}"
  route_table_id = "${aws_route_table.admin.id}"

  depends_on = ["aws_subnet.admin"]
}

#===============================================================================
# Security Groups
#===============================================================================
resource "aws_security_group" "vpn" {
  name        = "${var.environment_name}-${var.stack_id}-vpn"
  description = "Allow access from the ${var.environment_name}-${var.stack_id} VPN server"
  vpc_id      = "${aws_vpc.brian.id}"

  # This security group is used for allowing access to resources from the VPN
  # instance. It contains no rules, is attached to the VPN instance, and is
  # used in security groups for resources across the infrastructure.

  tags = "${merge(
    local.common_tags,
    map("Name", "${var.environment_name}-${var.stack_id}-vpn"),
  )}"
}

resource "aws_security_group" "nat" {
  name        = "${var.environment_name}-${var.stack_id}-nat"
  description = "Allow services from the ${var.environment_name}-${var.stack_id} private subnet through NAT instance"

  # ingress {
  #   from_port = 0
  #   to_port = 65535
  #   protocol = "tcp"
  #   cidr_blocks = ["${aws_subnet.a_private.cidr_block}"]
  # }
  # ingress {
  #   from_port = 0
  #   to_port = 65535
  #   protocol = "tcp"
  #   cidr_blocks = ["${aws_subnet.b_private.cidr_block}"]
  # }
  # ingress {
  #   from_port = 0
  #   to_port = 65535
  #   protocol = "tcp"
  #   cidr_blocks = ["${aws_subnet.c_private.cidr_block}"]
  # }

  vpc_id = "${aws_vpc.this_vpc.id}"
}
