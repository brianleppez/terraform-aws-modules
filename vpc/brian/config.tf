terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}

  # Most current terraform version available when creating this repo.
  required_version = "~> 0.11.13"
}

provider "aws" {
  # Most current aws version available when creating this repo.
  version = "~> 2.4.0"

  # Variable so that the region can be easily configured.
  region = "${var.aws_region}"
}
